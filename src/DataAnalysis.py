import pandas as pd
import os
import numpy as np
from pandas import ExcelWriter

Title = "Title"
Author = "Author"
Publisher = "Publisher"
Publication_Date = "Publication Date"
Price = "Price"
Product_Dimension = "Product Dimensions"
Pages = "Pages"
Type = "Type"
Age_Range = "Age Range"
Language = "Language"
Grade_Level = "Grade Level"
Shipping_Weight = "Shipping Weight"
ID = "Id"
LTABLE_ID = "ltable_Id"
RTABLE_ID = "rtable_Id"
ISBN_10 = "ISBN10"
ISBN_13 = "ISBN13"

# BNN
Edition_Description = "Edition Description"
Sales_Rank = "Sales Rank"
Average_Review = "Average Review"
Lexile = "Lexile"
Series = "Series"
Sold_By = "Sold By"
Format = "Format"

stage4_output_dir = os.getcwd() + os.sep + '..' + os.sep + 'output' + os.sep + 'Stage4'
merged_data_path = stage4_output_dir + os.sep + 'output.csv'
input_dataFrame = None

publisher_cleaning_map = {
    'Candlewick Press': 'Candlewick',
    'Chronicle Books LLP': 'Chronicle Books',
    'Henry Holt and Co. (BYR)': 'Henry Holt',
    'Little, Brown and Company': 'Little, Brown Books for Young Readers',
    'Morgan Reynolds Pub': 'Morgan Reynolds Incorporated',
    'Oxford University Press, USA': 'Oxford University Press',
    'Penguin Group (USA) Incorporated': 'Penguin Young Readers Group',
    'Scholastic Nonfiction': 'Scholastic Press',
    'Scholastic Paperbacks': 'Scholastic Press',
    'Scholastic, Inc.': 'Scholastic Press',
    'Simon Pulse/Beyond Words': 'Simon Pulse'
}


def analyzePagesAndTypes():
    local_data_frame = input_dataFrame
    local_data_frame[Pages].astype(str).astype(float)
    group_by_type = local_data_frame.groupby(Type)
    pages_vs_types = group_by_type[Pages].mean()

    # Plotting the value
    writeToExcel(pages_vs_types, "analyzePagesAndTypes")


def analyzePriceAndTypes():
    local_data_frame = input_dataFrame
    group_by_type = local_data_frame.groupby(Type)
    price_vs_types = group_by_type[Price].mean()
    writeToExcel(price_vs_types, "analyzePriceAndTypes")


def analyzePagesAndYear():
    local_data_frame = input_dataFrame
    year = []
    for index, row in local_data_frame.iterrows():
        # Keeping Just Year Part
        row[Publication_Date] = str(row[Publication_Date])[-4:]
        year.append(row[Publication_Date])

    local_data_frame[Publication_Date] = year
    local_data_frame[Pages].astype(str).astype(float)
    group_by_year = local_data_frame.groupby(Publication_Date)
    pages_vs_year = group_by_year[Pages].mean()
    writeToExcel(pages_vs_year[1:-1], "analyzePagesAndYear")


def analyseNumOfBooksAndYear():
    local_data_frame = input_dataFrame
    year = []
    for index, row in local_data_frame.iterrows():
        # Keeping Just Year Part
        row[Publication_Date] = str(row[Publication_Date])[-4:]
        year.append(row[Publication_Date])

    local_data_frame[Publication_Date] = year

    group_by_year = local_data_frame.groupby(Publication_Date)
    booksCountVsYear = group_by_year.size()[1:-1]

    writeToExcel(booksCountVsYear, "analyseNumOfBooksAndYear")


def analyseNumOfBooksAndMonth():
    month_list = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
                  'November', 'December']
    local_data_frame = input_dataFrame
    month = []
    delete_rows = []
    for index, row in local_data_frame.iterrows():
        # Keeping Just Year Part
        row[Publication_Date] = str(row[Publication_Date]).split()[0]
        if (row[Publication_Date] in month_list):
            month.append(row[Publication_Date])
        else:
            delete_rows.append(index)

    local_data_frame.drop(local_data_frame.index[delete_rows], inplace=True)
    local_data_frame[Publication_Date] = month

    group_by_month = local_data_frame.groupby(Publication_Date)
    booksCountVsMonth = group_by_month.size()
    df = booksCountVsMonth.to_frame()

    writeToExcel(df, "analyseNumOfBooksAndMonth")


def analysePriceAndYear():
    local_data_frame = input_dataFrame
    year = []
    for index, row in local_data_frame.iterrows():
        # Keeping Just Year Part
        row[Publication_Date] = str(row[Publication_Date])[-4:]
        year.append(row[Publication_Date])

    local_data_frame[Publication_Date] = year

    group_by_year = local_data_frame.groupby(Publication_Date)
    price_vs_year = group_by_year[Price].mean()

    writeToExcel(price_vs_year[1:-1], "analysePriceAndYear")


def analysePriceAndPublisher():
    local_data_frame = input_dataFrame
    group_by_publisher = local_data_frame.groupby(Publisher)
    price_vs_publisher = group_by_publisher[Price].mean()
    writeToExcel(price_vs_publisher, "analysePriceAndPublisher")


def analysePagesAndPublisher():
    local_data_frame = input_dataFrame
    group_by_publisher = local_data_frame.groupby(Publisher)
    pages_vs_publisher = group_by_publisher[Pages].mean()
    writeToExcel(pages_vs_publisher, "analysePagesAndPublisher")


def analyseBooksAndPrice():
    local_data_frame = input_dataFrame
    price_bucket = []

    for index, row in local_data_frame.iterrows():
        price = row[Price]
        if (price < 5):
            price_bucket.append("0-5");
        elif (price < 10):
            price_bucket.append("5-10");
        elif (price < 15):
            price_bucket.append("10-15");
        elif (price < 20):
            price_bucket.append("15-20");
        elif (price < 25):
            price_bucket.append("20-25");
        elif (price < 30):
            price_bucket.append("25-30");
        elif (price < 35):
            price_bucket.append("30-35");
        elif (price < 40):
            price_bucket.append("35-40");
        elif (price < 45):
            price_bucket.append("40-45");
        elif (price < 50):
            price_bucket.append("45-50");
        elif (price < 100):
            price_bucket.append("50-100");

    local_data_frame['Price Bucket'] = price_bucket

    group_by_price_bucket = local_data_frame.groupby('Price Bucket')
    books_vs_bucket = group_by_price_bucket.size()
    writeToExcel(books_vs_bucket, "analyseBooksAndPrice")


def writeToExcel(df, name):
    writer = ExcelWriter(stage4_output_dir + os.sep + name + '.xlsx')
    df.to_excel(writer, "Sheet1")
    writer.save()


def sanitize_publisher():
    publisher = []
    for index, row in input_dataFrame.iterrows():
        row[Publisher] = row[Publisher][1:-1].strip().split(';')[0].strip()
        if row[Publisher] in publisher_cleaning_map:
            row[Publisher] = publisher_cleaning_map[row[Publisher]]

        row[Publisher] = row[Publisher].upper()
        publisher.append(row[Publisher])
        # print row[Publisher]
    input_dataFrame[Publisher] = publisher


if __name__ == '__main__':
    input_dataFrame = pd.read_csv(merged_data_path)
    sanitize_publisher()
    # print input_dataFrame.dtypes
    # analyzePagesAndTypes()
    # analyzePriceAndTypes()
    # analyzePagesAndYear()
    # analyseNumOfBooksAndYear()
    # analyseNumOfBooksAndMonth()
    # analysePriceAndPublisher()
    # analysePriceAndYear()
    analysePagesAndPublisher()
    # analyseBooksAndPrice()
