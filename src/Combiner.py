import pandas as pd
import os

datasets_dir = os.getcwd() + os.sep + 'output' + os.sep + 'Before_Blocking'
stage4_output_dir = os.getcwd() + os.sep + 'output' + os.sep + 'Stage4'

Title = "Title"
Author = "Author"
Publisher = "Publisher"
Publication_Date = "Publication Date"
Price = "Price"
Product_Dimension = "Product Dimensions"
Pages = "Pages"
Type = "Type"
Age_Range = "Age Range"
Language = "Language"
Grade_Level = "Grade Level"
Shipping_Weight = "Shipping Weight"
ID = "Id"
LTABLE_ID = "ltable_Id"
RTABLE_ID = "rtable_Id"
ISBN_10 = "ISBN10"
ISBN_13 = "ISBN13"

# BNN
Edition_Description = "Edition Description"
Sales_Rank = "Sales Rank"
Average_Review = "Average Review"
Lexile = "Lexile"
Series = "Series"
Sold_By = "Sold By"
Format = "Format"


def merge_data(id, A_row, B_row):
    row = {}
    # Fill Id from both tuples:
    row[ID] = id
    # row[LTABLE_ID] = A_row[ID].iloc[0]
    # row[RTABLE_ID] = B_row[ID].iloc[0]
    # From Amazon
    row[Publication_Date] = A_row[Publication_Date].iloc[0]
    # From BNN
    row[Age_Range] = B_row[Age_Range].iloc[0]

    # From only one of the table based on missing values
    exists_in_one_list = [Type, Language, Grade_Level, Shipping_Weight, Edition_Description,
                          Sales_Rank, Lexile, Series, Sold_By, Format, Product_Dimension, ISBN_13, ISBN_10]
    for attr in exists_in_one_list:
        aval = A_row[attr].iloc[0]
        bval = B_row[attr].iloc[0]
        if (aval is None) or (aval is '') or (len(str(aval).strip()) == 0):
            row[attr] = bval
        elif (bval is None) or (bval is '') or (len(str(bval).strip()) == 0):
            row[attr] = aval
        else:
            pass
            # Empty in both

    large_len_attrs = [Title, Author, Publisher]
    for attr in large_len_attrs:
        aval = A_row[attr].iloc[0]
        bval = B_row[attr].iloc[0]
        if (aval is None) or (aval is '') or (len(str(aval).strip()) == 0):
            row[attr] = bval
        elif (bval is None) or (bval is '') or (len(str(bval).strip()) == 0):
            row[attr] = aval
        elif len(str(aval).strip()) < len(str(bval).strip()):
            row[attr] = bval
        else:
            row[attr] = aval

    numeric_attrs = [Pages, Price]
    # pick page number
    for attr in numeric_attrs:
        aval = A_row[attr].iloc[0]
        bval = B_row[attr].iloc[0]
        if attr == Price:
            if (aval is not None) and (len(str(aval).strip()) != 0):
                aval = float(str(aval).replace('$', ''))
            if (bval is not None) and (len(str(bval).strip()) != 0):
                bval = float(str(bval).replace('$', ''))
        if (aval is None) or (len(str(aval).strip()) == 0):
            row[attr] = bval
        elif (bval is None) or (len(str(bval).strip()) == 0):
            row[attr] = aval
        elif aval < bval:
            row[attr] = bval
        else:
            row[attr] = aval

    return [row]


def match_schemas():
    A = pd.read_csv(datasets_dir + os.sep + 'amazon.csv', keep_default_na=False)
    B = pd.read_csv(datasets_dir + os.sep + 'bnn.csv', keep_default_na=False)
    M = pd.read_csv(stage4_output_dir + os.sep + 'MatchedTuples.csv')
    columns = [ID, Title, Author, Publisher, Publication_Date, Price, Product_Dimension, Pages,
               Age_Range, Language, Type, Grade_Level, Shipping_Weight, Edition_Description,
               Sales_Rank, Lexile, Series, Sold_By, Format, ISBN_13, ISBN_10]
    Z = pd.DataFrame(columns=columns)
    count = 0
    for index, row in M.iterrows():
        aid = row[LTABLE_ID]
        bid = row[RTABLE_ID]
        A_row = A[A[ID] == aid]
        B_row = B[B[ID] == bid]
        output_row = pd.DataFrame(merge_data(count, A_row, B_row), columns=columns)
        Z = Z.append(output_row)
        count = count + 1
    Z.to_csv(stage4_output_dir + os.sep + 'output.csv', index=False)


if __name__ == '__main__':
    match_schemas()
